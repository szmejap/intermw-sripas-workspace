## Assumptions

For the following instructions to actually work, you need a working 64-bit `Linux` machine (we assume that it's a recent version of 64-bit `Ubuntu`). In addition `Git`, `JDK`, and `Maven` are required. If you need a 32-bit executable you have to compile Parliament from sources.

To start, you need to clone this repository:

```bash
git clone https://interiot@bitbucket.org/szmejap/intermw-sripas-workspace.git parliament-demo
```

We shall refer to the resulting `parliament-demo` directory as DEMODIR in what follows.

## [Parliament](http://parliament.semwebcentral.org/)

A pre-compiled distribution of `Parliament` triple store can be downloaded from [SemanticWebCentral](http://semwebcentral.org/frs/?group_id=159). You should choose an archive appropriate for your system configuration.

  - open a shell window
  - create `~/bin/parliament` directory
  - download [ParliamentQuickStart-v2.7.10-gcc-64-ubuntu-15.10.zip](http://semwebcentral.org/frs/download.php/555/ParliamentQuickStart-v2.7.10-gcc-64-ubuntu-15.10.zip)
  - unpack the ZIP archive into the `~/bin/parliament` directory
  - enter the DEMODIR
  - execute: `bash utl/start-parliament.sh`
  - **note**: to stop `Parliament` you should issue the `exit` command from the same shell window that you have started it.
  - open a web browser and point it to [http://localhost:8089/parliament/](http://localhost:8089/parliament/)


## Sample datasets
### Kno.e.sis dataset


LinkedSensorData is an RDF dataset containing expressive descriptions of weather stations in the US. On average, there are about five sensors per weather station measuring phenomena such as temperature, visibility, precipitation, pressure, wind speed, humidity, etc. In addition to location attributes such as latitude, longitude, and elevation, there are also links to locations in Geonames that are near each weather station.

```
@prefix om-owl:     <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#> .
@prefix rdfs:       <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sens-obs:   <http://knoesis.wright.edu/ssw/> .
@prefix owl:        <http://www.w3.org/2002/07/owl#> .
@prefix xsd:        <http://www.w3.org/2001/XMLSchema#> .
@prefix weather:    <http://knoesis.wright.edu/ssw/ont/weather.owl#> .
@prefix rdf:        <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix wgs84:      <http://www.w3.org/2003/01/geo/wgs84_pos#> .

sens-obs:point_A01
      a       wgs84:Point ;
      wgs84:alt "4149"^^xsd:float ;
      wgs84:lat "37.0339"^^xsd:float ;
      wgs84:long "-116.0989"^^xsd:float .

sens-obs:System_A01
      a       om-owl:System ;
      om-owl:ID "A01" ;
      om-owl:hasLocatedNearRel
              sens-obs:LocatedNearRelA01 ;
      om-owl:hasSourceURI <http://mesowest.utah.edu/cgi-bin/droman/meso_base.cgi?stn=A01> ;
      om-owl:parameter weather:_WindDirection , weather:_RelativeHumidity , weather:_DewPoint , weather:_AirTemperature , weather:_WindSpeed , weather:_WindGust ;
      om-owl:processLocation
              sens-obs:point_A01 .

sens-obs:LocatedNearRelA01
      a       om-owl:LocatedNearRel ;
      om-owl:distance "0.5362"^^xsd:float ;
      om-owl:hasLocation <http://sws.geonames.org/5509122/> ;
      om-owl:uom weather:miles .`
```

To import the Knoesis dataset to Parliament take the following steps:

  - enter the DEMODIR
  - execute: `bash utl/import-knoesis.sh`
  - from the `Parliament` web UI choose [Indexes](http://localhost:8089/parliament/indexes.jsp) and select `createAll` for the `http://knoesis` graph.

### LinkedGeoData

The LinkedGeoData dataset is a large spatial knowledge base which has been derived from Open Street Map. The used dataset contains data about roads.

```
@prefix lgdo:   <http://linkedgeodata.org/ontology/> .
@prefix rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:   <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml:    <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:    <http://www.w3.org/2001/XMLSchema#> .

lgdo:asWKT a <http://www.w3.org/2002/07/owl#DatatypeProperty> ;
    rdfs:subPropertyOf <http://www.opengis.net/ont/geosparql#asWKT> .

lgdo:hasGeometry a <http://www.w3.org/2002/07/owl#ObjectProperty> ;
    rdfs:subPropertyOf <http://www.opengis.net/ont/geosparql#hasGeometry> .

<http://linkedgeodata.org/triplify/way100003492> a <http://linkedgeodata.org/meta/Way>,
        lgdo:HighwayThing,
        lgdo:Motorway,
        <http://www.opengis.net/ont/geosparql#Feature> ;
    lgdo:bridge true ;
    lgdo:hasGeometry <http://linkedgeodata.org/geometry/way100003492> ;
    lgdo:int_ref "E 94" ;
    lgdo:lanes "3"^^xsd:int ;
    lgdo:layer "1"^^xsd:int ;
    lgdo:oneway true .

<http://linkedgeodata.org/geometry/way100003492> a <http://www.opengis.net/ont/sf#LineString> ;
    lgdo:asWKT "<http://www.opengis.net/def/crs/EPSG/0/4326> LINESTRING(23.231697500000003 37.9757906,23.230942900000002 37.9754912)"^^<http://www.opengis.net/ont/geosparql#wktLiteral> .
```

To import the LinkedGeoData dataset into `Parliament` perform the following actions:

  - enter the DEMODIR
  - execute: `bash utl/import-lgd.sh`
  - from the `Parliament` web UI choose [Indexes](http://localhost:8089/parliament/indexes.jsp) and select `createAll` for the `http://lgd` graph.

  Choosing [Explore](http://localhost:8089/parliament/explorerIndex.jsp) from the `Parliament` web UI menu, at the bottom of the page you should see the list of available "Graphs". It should (among others) contain `http://knoesis` and `http://lgd`, which we have created above.


## Sample queries

#### Knoesis: Count triples (use [Query](http://localhost:8089/parliament/query.jsp) panel)

Count devices that were loaded to the triple store named graph <http://knoesis>.

```
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT COUNT(*)
WHERE {
    GRAPH <http://knoesis> {?s a om-owl:System}
}
```

#### Knoesis: Get sample devices with W3C Geo data representation  (use [Query](http://localhost:8089/parliament/query.jsp) panel)

The query returns 10 random devices with their geographic coordinates.

```
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>
PREFIX wgs84:   <http://www.w3.org/2003/01/geo/wgs84_pos#>

SELECT
?s ?p ?alt ?lat ?long
WHERE {
    GRAPH <http://knoesis> {
        ?s a om-owl:System .
        ?s om-owl:processLocation ?p .
        ?p wgs84:alt ?alt .
        ?p wgs84:lat ?lat .
        ?p wgs84:long ?long .
    }
} LIMIT 10
```

#### Knoesis: Generate GeoSPARQL spatial data (use [SPARQL/Update](http://localhost:8089/parliament/update.jsp) panel)

The W3C Geo data representation is a widely used simple representation for point data. There is a straightforward process to convert data from this format into GeoSPARQL. It is simply a matter of concatenating the longitude and latitude into a WKT point.
The default coordinate reference system in WKT literals is http://www.opengis.net/def/crs/OGC/1.3/CRS84 i.e. WGS84 with a longitude latitude order.  To use a different CRS in a WKT literal, it should be prepended in angle brackets at the beginning of the literal e.g.
"<http://www.opengis.net/def/crs/EPSG/0/4326> POINT(38.889468 -77.03524)"^^geo:WktLiteral.

```
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX wgs84_pos:   <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>
PREFIX sf: <http://www.opengis.net/ont/sf/>

INSERT INTO <http://knoesis>  {
    ?sys a geo:Feature ;
    geo:hasGeometry [
        a sf:Point ;
        geo:asWKT ?wkt
    ] .
}
WHERE {
    GRAPH <http://knoesis> {
        ?sys om-owl:processLocation ?point .
        ?point a wgs84_pos:Point .
        ?point wgs84_pos:lat ?lat .
        ?point wgs84_pos:long ?long .
        BIND (STRDT(CONCAT("POINT(",STR(?long), " ", STR(?lat), ")"),geo:wktLiteral) as ?wkt) .
    }
}
```

#### Knoesis: Get sample devices with GeoSPARQL spatial data (use [Query](http://localhost:8089/parliament/query.jsp) panel)

The query returns 10 random sensors with geospatial data. There are three key classes in the GeoSPARQL ontology:
- geo:Feature – a thing that can have a spatial location,
- geo:Geometry – a representation of a spatial location,
- geo:SpatialObject – a superclass of both Features and Geometries.

The geo:hasGeometry property links Feature (a thing) to its Geometry (location). The geometry has an RDF literal representation, which is linked with a property named for the type of representation e.g. geo:asWKT.

```
PREFIX geo: <http://www.opengis.net/ont/geosparql#>

SELECT
?s ?p ?o
WHERE {
    GRAPH <http://knoesis> {
        ?s a geo:Feature .
        ?s geo:hasGeometry ?p .
        ?p geo:asWKT ?o .
    }
} LIMIT 10
```

#### Knoesis: Get device by coordinates (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT ?s1 ?id1 ?o1
WHERE {
    GRAPH <http://knoesis> {
        ?s1 geo:hasGeometry ?g1 .
        ?s1 om-owl:ID ?id1 .
        ?g1 geo:asWKT ?o1
    }
    FILTER(geof:sfEquals(?o1, "POINT(-115.99650 37.05183)" ^^<http://www.opengis.net/ont/geosparql#wktLiteral>)).
}
```

#### Knoesis: Get devices located within a given polygon (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT ?n1 ?s1 ?g1  ?o1
WHERE {
    GRAPH <http://knoesis> {
        ?s1 geo:hasGeometry ?g1 .
        ?s1 om-owl:ID ?id1 .
        ?g1 geo:asWKT ?o1
    }
    FILTER(geof:sfWithin(?o1, "<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON ((-77.2 38.8, -77 38.8, -77 39, -77.2 39.9, -77.2 38.8))" ^^<http://www.opengis.net/ont/geosparql#wktLiteral>)).
} limit 10
```

#### Knoesis: Get devices within a given distance from a point (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX units: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT ?s1 ?id1 ?o1
WHERE {
    GRAPH <http://knoesis> {
        ?s1 geo:hasGeometry ?g1 .
        ?s1 om-owl:ID ?id1 .
        ?g1 geo:asWKT ?o1
    }
    FILTER(geof:distance(?o1, "POINT(-116.0989 37.0339)" ^^<http://www.opengis.net/ont/geosparql#wktLiteral>, units:metre) <= 10000).
}
```

#### Knoesis: Get devices outside a given polygon (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT ?s1 ?id1 ?o1
WHERE {
    GRAPH <http://knoesis> {
        ?s1 geo:hasGeometry ?g1 .
        ?s1 om-owl:ID ?id1 .
        ?g1 geo:asWKT ?o1
    }
    FILTER(geof:sfDisjoint(?o1, "<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POLYGON ((-77.2 38.8, -77 38.8, -77 39, -77.2 39.9, -77.2 38.8))"^^<http://www.opengis.net/ont/geosparql#wktLiteral>)).
}  LIMIT 10
```

#### Knoesis: Get devices with a given point buffer (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX units: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>

SELECT ?s1 ?id1 ?o1
WHERE {
    GRAPH <http://knoesis> {
        ?s1 geo:hasGeometry ?g1 .
        ?s1 om-owl:ID ?id1 .
        ?g1 geo:asWKT ?o1
    }
    FILTER(geof:sfWithin(?o1, geof:buffer("POINT(-116.0989 37.0339)" ^^<http://www.opengis.net/ont/geosparql#wktLiteral>, 10000, units:metre))).
} limit 10
```

#### LinkedGeoData: Get objects that are Features from a graph (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geo: <http://www.opengis.net/ont/geosparql#>

SELECT DISTINCT
?s
WHERE {
    GRAPH <http://lgd> {?s a geo:Feature .}
} LIMIT 10
```

#### LinkedGeoData: Get object with line geometry by coordinates (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX lgd: <http://linkedgeodata.org/ontology/>

SELECT ?s1 ?o1
WHERE {
	GRAPH <http://lgd> {?s1 lgd:asWKT ?o1}
	FILTER(geof:sfEquals(?o1, "<http://www.opengis.net/def/crs/EPSG/0/4326> LINESTRING(23.231697500000003 37.9757906,23.230942900000002 37.9754912)"^^<http://www.opengis.net/ont/geosparql#wktLiteral>)).
}
```

#### LinkedGeoData: Get roads that cross a given line (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX lgd: <http://linkedgeodata.org/ontology/>

SELECT ?s1 ?n1 ?g1
WHERE {
    GRAPH <http://lgd> {
        ?s1 lgd:hasGeometry ?g1 .
        optional {?s1 rdf:type ?n1 . }
        ?g1 lgd:asWKT ?o1
     }
	FILTER(geof:sfCrosses(?o1, "<http://www.opengis.net/def/crs/EPSG/0/4326> LINESTRING(25.231642800000003 37.975887400000005,23.2308844 37.9756318)"^^<http://www.opengis.net/ont/geosparql#wktLiteral>)).
} LIMIT 10
```

#### LinkedGeoData: Get roads that intersect a given polygon (use [Query](http://localhost:8089/parliament/query.jsp) panel)

```
PREFIX dataset: <http://geographica.di.uoa.gr/dataset/>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX lgd: <http://linkedgeodata.org/ontology/>

SELECT ?s1 ?o1
WHERE {
	GRAPH <http://lgd> {?s1 lgd:asWKT ?o1}
	FILTER(geof:sfIntersects(?o1, "<http://www.opengis.net/def/crs/EPSG/0/4326> POLYGON ((-180 38.8, 180 38.8, 180 39, -180 39.9, -180 38.8))"^^geo:wktLiteral)).
} LIMIT 10
```

### GeoSPRARQL functions summary

- geof:sfEquals
- geof:sfDisjoint
- geof:sfIntersects
- geof:sfTouches
- geof:sfWithin
- geof:sfContains
- geof:sfOverlaps
- geof:sfCrosses
- geof:distance
- geof:buffer
- geof:convexHull
- geof:intersection
- geof:union
- geof:difference
- geof:symDifference
- geof:envelope
- geof:boundary
- geof:getsrid

### Prefixes

- PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
- PREFIX geo: <http://www.opengis.net/ont/geosparql#>
- PREFIX sf: <http://www.opengis.net/ont/sf/>
- PREFIX units: <http://www.opengis.net/def/uom/OGC/1.0/>
- PREFIX om-owl:  <http://knoesis.wright.edu/ssw/ont/sensor-observation.owl#>
- PREFIX wgs84:   <http://www.w3.org/2003/01/geo/wgs84_pos#>
- PREFIX lgd: <http://linkedgeodata.org/ontology/>
