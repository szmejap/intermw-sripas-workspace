#!/bin/bash
mkdir sandbox
cd sandbox
tar xzf ../datasets/knoesis.tar.gz
git clone https://interiot@bitbucket.org/interiot/parliament-import.git
cd parliament-import
mvn install:install-file compile assembly:single
mv target/parliament-import-1.0-jar-with-dependencies.jar ../parliament-import.jar
cd ..
java -jar parliament-import.jar --graph=http://knoesis --dir=knoesis

